use std::fs;
use std::io::Read;
use std::io;

pub fn read_to_bytes(filename: &str) -> io::Result<Vec<u8>> {
    let mut file = fs::File::open(filename)?;
    let mut bytes = vec![];
    let mut buf = [0; 256];
    loop {
        match file.read(&mut buf)? {
            0 => break,
            n => bytes.extend_from_slice(&buf[0..n]),
        }
    }
    dbg!(bytes.len());
    Ok(bytes)
}

pub fn load_fontset() -> Vec<u8> {
    let mut res = vec![];
    // 0
    res.extend_from_slice(&[
        0b1111 << 4,
        0b1001 << 4,
        0b1001 << 4,
        0b1001 << 4,
        0b1111 << 4,
    ]);
    
    // 1
    res.extend_from_slice(&[
        0b0010 << 4,
        0b0110 << 4,
        0b0010 << 4,
        0b0010 << 4,
        0b0111 << 4,
    ]);
    
    // 2
    res.extend_from_slice(&[
        0b1111 << 4,
        0b0001 << 4,
        0b1111 << 4,
        0b1000 << 4,
        0b1111 << 4,
    ]);

    // 3
    res.extend_from_slice(&[
        0b1111 << 4,
        0b0001 << 4,
        0b1111 << 4,
        0b0001 << 4,
        0b1111 << 4,
    ]);

    // 4
    res.extend_from_slice(&[
        0b1001 << 4,
        0b1001 << 4,
        0b1111 << 4,
        0b0001 << 4,
        0b0001 << 4,
    ]);

    // 5
    res.extend_from_slice(&[
        0b1111 << 4,
        0b1000 << 4,
        0b1111 << 4,
        0b0001 << 4,
        0b1111 << 4,
    ]);

    // 6
    res.extend_from_slice(&[
        0b1111 << 4,
        0b1000 << 4,
        0b1111 << 4,
        0b1001 << 4,
        0b1111 << 4,
    ]);

    // 7
    res.extend_from_slice(&[
        0b1111 << 4,
        0b0001 << 4,
        0b0001 << 4,
        0b0001 << 4,
        0b0001 << 4,
    ]);

    // 8
    res.extend_from_slice(&[
        0b1111 << 4,
        0b1001 << 4,
        0b1111 << 4,
        0b1001 << 4,
        0b1111 << 4,
    ]);

    // 9
    res.extend_from_slice(&[
        0b1111 << 4,
        0b1001 << 4,
        0b1111 << 4,
        0b0001 << 4,
        0b1111 << 4,
    ]);

    // A
    res.extend_from_slice(&[
        0b1111 << 4,
        0b1001 << 4,
        0b1111 << 4,
        0b1001 << 4,
        0b1001 << 4,
    ]);

    // B
    res.extend_from_slice(&[
        0b1110 << 4,
        0b1001 << 4,
        0b1110 << 4,
        0b1001 << 4,
        0b1110 << 4,
    ]);

    // C
    res.extend_from_slice(&[
        0b0110 << 4,
        0b1001 << 4,
        0b1000 << 4,
        0b1001 << 4,
        0b0110 << 4,
    ]);

    // D
    res.extend_from_slice(&[
        0b1110 << 4,
        0b1001 << 4,
        0b1001 << 4,
        0b1001 << 4,
        0b1110 << 4,
    ]);

    // E
    res.extend_from_slice(&[
        0b1111 << 4,
        0b1000 << 4,
        0b1110 << 4,
        0b1000 << 4,
        0b1111 << 4,
    ]);

    // F
    res.extend_from_slice(&[
        0b1111 << 4,
        0b1000 << 4,
        0b1110 << 4,
        0b1000 << 4,
        0b1000 << 4,
    ]);

    res
}