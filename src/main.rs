use chip_8::emulator::CHIP8Emulator;

use serde::Deserialize;
use docopt::Docopt;
use ggez::{
    ContextBuilder,
    event,
    conf::Conf,
};

use std::error::Error;

const USAGE: &'static str = "
Rust CHIP-8 TUI.

Just another one CHIP-8 implementation in Rust. Outputs graphics into the terminal.

Usage:
    rust-chip8 run <game>
    rust-chip8 (-h | --help)
    rust-chip8 --version

Options:
    -h --help  Show this screen.
    --version  Show version.
";

#[derive(Debug, Deserialize)]
struct Args {
    arg_game: Option<String>,
    cmd_run: bool,
}

fn main() -> Result<(), Box<dyn Error>> {
    let args: Args = Docopt::new(USAGE)
        .and_then(|dopt| dopt.deserialize())
        .unwrap_or_else(|e| e.exit());
    
    if !args.cmd_run {
        panic!("Unexpected input, args: {:?}", args)
    }
    // let args = Args{
    //     arg_game: Some("games/slipperyslope.ch8".into()),
    //     cmd_run: true,
    // };

    // Start the game
    let mut conf = Conf::new();
    conf.window_setup.vsync = false;
    let (mut ctx, mut event_loop) = ContextBuilder::new("CHIP-8 Emulator", "Apachee")
        .conf(conf)
        .build()
        .unwrap();
    
    let mut chip8 = CHIP8Emulator::new(&mut ctx)?;
    chip8.load_fontset();
    chip8.load_rom(&args.arg_game.expect("Unexpected unspecified argument <game>"))?;

    match event::run(&mut ctx, &mut event_loop, &mut chip8) {
        Ok(_) => println!("Exited cleanly."),
        Err(e) => println!("Error occured: {}", e)
    }

    Ok(())
}
