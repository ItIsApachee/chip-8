use std::{
    io,
    collections::HashMap,
    cmp::min,
    time,
};

use ggez::{
    Context,
    GameResult,
    event::{
        self,
        EventHandler,
    },
    input::keyboard::{
        self,
        KeyCode,
        KeyMods,
    },
    graphics,
    audio::{
        self,
        SoundSource,
    },
};

use super::util::*;
use super::opcode::*;

const COLS: u8 = 64;
const ROWS: u8 = 32;
const BEEP_SOUND: &'static [u8] = include_bytes!("../res/beep.wav");

#[derive(Debug, Clone)]
enum AwaitingStatus {
    None,
    Awaiting(usize),
    AwaitingScreenUpdate,
}

pub struct CHIP8Emulator {
    memory: [u8; 4096],

    regs: [u8; 16],
    index_reg: u16,

    program_counter: u16,
    stack: Vec<u16>,

    internal_timer: time::Instant,
    last_tick: u64,
    delay_timer: u8,
    sound_timer: u8,
    beep_sound: audio::Source,

    awaiting_status: AwaitingStatus,
    keymap: HashMap<u8, KeyCode>,
    keymap_rev: HashMap<KeyCode, u8>,

    screen: [bool; COLS as usize * ROWS as usize],
}

impl CHIP8Emulator {
    pub fn new(ctx: &mut Context) -> Result<Self, String> {
        let keypairs = vec![
            KeyCode::X, // 0
            KeyCode::Key1, // 1
            KeyCode::Key2, // 2
            KeyCode::Key3, // 3
            KeyCode::Q, // 4
            KeyCode::W, // 5
            KeyCode::E, // 6
            KeyCode::A, // 7
            KeyCode::S, // 8
            KeyCode::D, // 9
            KeyCode::Z, // A
            KeyCode::C, // B
            KeyCode::Key4, // C
            KeyCode::R, // D
            KeyCode::F, // E
            KeyCode::V, // F
        ];

        let mut beep_sound = audio::Source::from_data(
            ctx, 
            audio::SoundData::from_bytes(BEEP_SOUND),
        ).map_err(|_| "can't load sound")?;
        beep_sound.set_repeat(true);
        beep_sound.set_volume(0.05);

        Ok(
            CHIP8Emulator {
                memory: [0; 4096],
    
                regs: [0; 16],
                index_reg: 0x0,
    
                program_counter: 0x200,
                stack: vec![],
    
                internal_timer: time::Instant::now(),
                last_tick: 0,
                delay_timer: 0,
                sound_timer: 0,
                beep_sound,
    
                awaiting_status: AwaitingStatus::None,
                keymap: keypairs.clone()
                    .into_iter()
                    .enumerate()
                    .map(|(a, b)| {
                        (a as u8, b)
                    })
                    .collect(),
                keymap_rev: keypairs.into_iter()
                    .enumerate()
                    .map(|(a, b)| {
                        (b, a as u8)
                    })
                    .collect(),
    
                screen: [false; COLS as usize * ROWS as usize],
            }
        )
    }

    fn get_pixel(&mut self, x: u8, y: u8) -> usize {
        assert!(y < ROWS && x < COLS, format!("x ({}) or y ({}) too big", x, y));
        y as usize * COLS as usize + x as usize
    }

    pub fn load_rom(&mut self, filename: &str) -> io::Result<()> {
        // Load file into the memory
        let bytes = read_to_bytes(filename)?;
        assert!(bytes.len() <= 4096, format!("Too big file for a game, 4096 limit exceeded ({})", bytes.len()));
        for (memory_byte, file_byte) in self.memory
            .iter_mut()
            .skip(0x200)
            .zip(bytes) {
            *memory_byte = file_byte;
        }
        Ok(())
    }

    pub fn load_fontset(&mut self) {
        // Load default fontset using util function
        let bytes = load_fontset();
        assert!(bytes.len() < 0x200, format!("Fontset exceeds 0x200 bytes limit ({})", bytes.len()));
        for (memory_byte, fontset_byte) in self.memory
            .iter_mut()
            .take(0x200)
            .zip(bytes) {
            *memory_byte = fontset_byte;
        }
    }

    fn fetch(&mut self) -> u16 {
        assert!(self.program_counter < 4096, "Program counter jumped over boundaries");
        let pc = self.program_counter as usize;
        let opcode = [
            self.memory[pc],
            self.memory[pc + 1],
        ];
        let ret = u16::from_be_bytes(opcode);
        ret
    }

    fn execute(&mut self, ctx: &mut Context, opcode: &CHIP8Opcode) -> io::Result<()> {
        use CHIP8Opcode::*;
        match *opcode {
            ClearScreen => self.opcode_clear_screen(),
            DrawSprite{reg_x, reg_y, height} => self.opcode_draw_sprite(reg_x, reg_y, height),
            Return => self.opcode_return(),
            Goto(addr) => self.opcode_goto(addr),
            CallSubroutine(addr) => self.opcode_call_subroutine(addr),
            SkipNextIfEqual{reg_id, value} => self.opcode_skip_next_if_equal(reg_id, value),
            SkipNextIfNEqual{reg_id, value} => self.opcode_skip_next_if_nequal(reg_id, value),
            SkipNextIfRegEqual{reg_a, reg_b} => self.opcode_skip_next_if_reg_equal(reg_a, reg_b),
            SkipNextIfRegNEqual{reg_a, reg_b} => self.opcode_skip_next_if_reg_nequal(reg_a, reg_b),
            SkipNextIfKeyEqual{reg_id} => self.opcode_skip_next_if_key_equal(ctx, reg_id),
            SkipNextIfKeyNEqual{reg_id} => self.opcode_skip_next_if_key_nequal(ctx, reg_id),
            AssignValue{reg_id, value} => self.opcode_assign_value(reg_id, value),
            AssignAddValue{reg_id, value} => self.opcode_assign_add_value(reg_id, value),
            AssignReg{reg_a, reg_b} => self.opcode_assign_reg(reg_a, reg_b),
            AssignBitwiseOr{reg_a, reg_b} => self.opcode_assign_bitwise_or(reg_a, reg_b),
            AssignBitwiseAnd{reg_a, reg_b} => self.opcode_assign_bitwise_and(reg_a, reg_b),
            AssignBitwiseXor{reg_a, reg_b} => self.opcode_assign_bitwise_xor(reg_a, reg_b),
            AssignAdd{reg_a, reg_b} => self.opcode_assign_add(reg_a, reg_b),
            AssignSub{reg_a, reg_b} => self.opcode_assign_sub(reg_a, reg_b),
            AssignBitwiseRShift{reg_a, ..} => self.opcode_assign_bitwise_rshift(reg_a),
            AssignBitwiseLShift{reg_a, ..} => self.opcode_assign_bitwise_lshift(reg_a),
            AssignSubReverse{reg_a, reg_b} => self.opcode_assign_sub_reverse(reg_a, reg_b),
            AssignDelayTimer{reg_id} => self.opcode_assign_delay_timer(reg_id),
            AssignToDelayTimer{reg_id} => self.opcode_assign_to_delay_timer(reg_id),
            AssignToSoundTimer{reg_id} => self.opcode_assign_to_sound_timer(reg_id),
            AssignToI(addr) => self.opcode_assign_to_i(addr),
            AssignAddToI{reg_id} => self.opcode_assign_add_to_i(reg_id),
            AssignGlyphToI{reg_id} => self.opcode_assign_glyph_to_i(reg_id),
            BinaryCodedDecimal{reg_id} => self.opcode_binary_coded_decimal(reg_id),
            RegDump{reg_id} => self.opcode_reg_dump(reg_id),
            RegLoad{reg_id} => self.opcode_reg_load(reg_id),
            AwaitKey{reg_id} => self.opcode_await_key(reg_id),
            GotoPlusV0(addr) => self.opcode_goto_plus_v0(addr),
            RandWithBitwiseAnd{reg_id, value} => self.opcode_rand_with_bitwise_and(reg_id, value),
            UnknownOpcode(opcode) => panic!("Unknown opcode: {:x}", opcode),
        }
        Ok(())
    }

    fn opcode_clear_screen(&mut self) {
        // Clears the screen.
        for i in self.screen.iter_mut() {
            *i = false;
        }
        self.awaiting_status = AwaitingStatus::AwaitingScreenUpdate;
        self.program_counter += 2;
    }

    fn opcode_draw_sprite(&mut self, reg_x: usize, reg_y: usize, height: u8) {
        // Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels 
        // and a height of N pixels. Each row of 8 pixels is read as bit-coded 
        // starting from memory location I; I value doesn’t change after the 
        // execution of this instruction. As described above, VF is set to 1 
        // if any screen pixels are flipped from set to unset when the sprite 
        // is drawn, and to 0 if that doesn’t happen.
        let mut height = height;
        if height == 0 {
            height = 16;
        }
        self.regs[0xF] = 0;
        let (x, y) = (self.regs[reg_x] % COLS, self.regs[reg_y] % ROWS);
        for offset_y in 0..height {
            let byte_addr = self.index_reg as usize + offset_y as usize;
            for offset_x in 0..8 {
                if self.memory[byte_addr] & (0x80 >> offset_x) != 0 {
                    let pixel_addr = self.get_pixel(
                        ((x as u16 + offset_x as u16) % COLS as u16) as u8, 
                        ((y as u16 + offset_y as u16) % ROWS as u16) as u8,
                    );
                    let pixel = &mut self.screen[pixel_addr];
                    if *pixel {
                        self.regs[0xF] = 1;
                    }
                    *pixel ^= true;
                }
            }
        }
        self.awaiting_status = AwaitingStatus::AwaitingScreenUpdate;
        self.program_counter += 2;
    }

    fn opcode_return(&mut self) {
        // Returns from a subroutine. 
        let ret_addr = self.stack.pop().expect("empty stack");
        self.opcode_goto(ret_addr);
    }

    fn opcode_goto(&mut self, addr: u16) {
        // Jumps to address NNN. 
        self.program_counter = addr;
    }

    fn opcode_call_subroutine(&mut self, addr: u16) {
        // Calls subroutine at NNN. 
        self.program_counter += 2;
        self.stack.push(self.program_counter);
        self.opcode_goto(addr);
    }

    fn opcode_skip_next_if_equal(&mut self, reg_id: usize, value: u8) {
        let reg_val = self.regs[reg_id];
        if reg_val == value {
            self.program_counter += 2;
        }
        self.program_counter += 2;
    }

    fn opcode_skip_next_if_nequal(&mut self, reg_id: usize, value: u8) {
        let reg_val = self.regs[reg_id];
        if reg_val != value {
            self.program_counter += 2;
        }
        self.program_counter += 2;
    }

    fn opcode_skip_next_if_reg_equal(&mut self, reg_a: usize, reg_b: usize) {
        let (reg_a, reg_b) = (self.regs[reg_a], self.regs[reg_b]);
        if reg_a == reg_b {
            self.program_counter += 2;
        }
        self.program_counter += 2;
    }
    
    fn opcode_skip_next_if_reg_nequal(&mut self, reg_a: usize, reg_b: usize) {
        let (reg_a, reg_b) = (self.regs[reg_a], self.regs[reg_b]);
        if reg_a != reg_b {
            self.program_counter += 2;
        }
        self.program_counter += 2;
    }
    
    fn opcode_skip_next_if_key_equal(&mut self, ctx: &mut Context, reg_id: usize) {
        let key = self.keymap[&self.regs[reg_id]];
        if keyboard::is_key_pressed(ctx, key) {
            self.program_counter += 2;
        } else {
        }
        self.awaiting_status = AwaitingStatus::AwaitingScreenUpdate;
        self.program_counter += 2;
    }
    
    fn opcode_skip_next_if_key_nequal(&mut self, ctx: &mut Context, reg_id: usize) {
        let key = self.keymap[&self.regs[reg_id]];
        if !keyboard::is_key_pressed(ctx, key) {
            self.program_counter += 2;
        } else {
        }
        self.awaiting_status = AwaitingStatus::AwaitingScreenUpdate;
        self.program_counter += 2;
    }
    
    fn opcode_assign_value(&mut self, reg_id: usize, value: u8) {
        self.regs[reg_id] = value;
        self.program_counter += 2;
    }
    
    fn opcode_assign_add_value(&mut self, reg_id: usize, value: u8) {
        let result = self.regs[reg_id].overflowing_add(value);
        self.regs[reg_id] = result.0;
        self.program_counter += 2;
    }
    
    fn opcode_assign_reg(&mut self, reg_a: usize, reg_b: usize) {
        self.regs[reg_a] = self.regs[reg_b];
        self.program_counter += 2;
    }
    
    fn opcode_assign_bitwise_or(&mut self, reg_a: usize, reg_b: usize) {
        self.regs[reg_a] |= self.regs[reg_b];
        self.program_counter += 2
    }
    
    fn opcode_assign_bitwise_and(&mut self, reg_a: usize, reg_b: usize) {
        self.regs[reg_a] &= self.regs[reg_b];
        self.program_counter += 2
    }
    
    fn opcode_assign_bitwise_xor(&mut self, reg_a: usize, reg_b: usize) {
        self.regs[reg_a] ^= self.regs[reg_b];
        self.program_counter += 2
    }
    
    fn opcode_assign_add(&mut self, reg_a: usize, reg_b: usize) {
        let result = self.regs[reg_a].overflowing_add(self.regs[reg_b]);
        self.regs[reg_a] = result.0;
        self.regs[0xF] = if result.1 {1} else {0};
        self.program_counter += 2;
    }
    
    fn opcode_assign_sub(&mut self, reg_a: usize, reg_b: usize) {
        let result = self.regs[reg_a].overflowing_sub(self.regs[reg_b]);
        self.regs[reg_a] = result.0;
        self.regs[0xF] = if result.1 {1} else {0};
        self.program_counter += 2;
    }
    
    fn opcode_assign_bitwise_rshift(&mut self, reg_a: usize) {
        self.regs[0xF] = self.regs[reg_a] & 1;
        self.regs[reg_a] >>= 1;
        self.program_counter += 2;
    }
    
    fn opcode_assign_bitwise_lshift(&mut self, reg_a: usize) {
        self.regs[0xF] = self.regs[reg_a] & 0b10000000;
        self.regs[reg_a] <<= 1;
        self.program_counter += 2;
    }
    
    fn opcode_assign_sub_reverse(&mut self, reg_a: usize, reg_b: usize) {
        let result = self.regs[reg_b].overflowing_sub(self.regs[reg_a]);
        self.regs[reg_a] = result.0;
        self.regs[0xF] = if result.1 {1} else {0};
        self.program_counter += 2;
    }
    
    fn opcode_assign_delay_timer(&mut self, reg_id: usize) {
        self.regs[reg_id] = self.delay_timer;
        self.program_counter += 2;
    }
    
    fn opcode_assign_to_delay_timer(&mut self, reg_id: usize) {
        self.delay_timer = self.regs[reg_id];
        self.program_counter += 2;
    }
    
    fn opcode_assign_to_sound_timer(&mut self, reg_id: usize) {
        self.sound_timer = self.regs[reg_id];
        self.program_counter += 2;
    }
    
    fn opcode_assign_to_i(&mut self, addr: u16) {
        self.index_reg = addr % 4096;
        self.program_counter += 2;
    }
    
    fn opcode_assign_add_to_i(&mut self, reg_id: usize) {
        let offset = self.regs[reg_id];
        self.index_reg += offset as u16;
        self.index_reg %= 4096;
        self.program_counter += 2;
    }
    
    fn opcode_assign_glyph_to_i(&mut self, reg_id: usize) {
        // todo!("implement fontset loading and this opcode");
        self.index_reg = self.regs[reg_id] as u16 * 5;
        self.program_counter += 2;
    }
    
    fn opcode_binary_coded_decimal(&mut self, reg_id: usize) {
        let mut reg_val = self.regs[reg_id];
        for i in 0..3 {
            self.memory[self.index_reg as usize + (2 - i)] = reg_val % 10;
            reg_val /= 10;
        }
        self.program_counter += 2;
    }
    
    fn opcode_reg_dump(&mut self, reg_id: usize) {
        for i in 0..=reg_id {
            self.memory[self.index_reg as usize + i] = self.regs[i];
        }
        self.program_counter += 2;
    }
    
    fn opcode_reg_load(&mut self, reg_id: usize) {
        for i in 0..=reg_id {
            self.regs[i] = self.memory[self.index_reg as usize + i];
        }
        self.program_counter += 2;
    }
    
    fn opcode_await_key(&mut self, reg_id: usize) {
        self.awaiting_status = AwaitingStatus::Awaiting(reg_id);
        self.program_counter += 2;
    }
    
    fn opcode_goto_plus_v0(&mut self, addr: u16) {
        self.opcode_goto(addr + self.regs[0x0] as u16);
    }
    
    fn opcode_rand_with_bitwise_and(&mut self, reg_id: usize, value: u8) {
        self.regs[reg_id] = rand::random::<u8>() & value; 
        self.program_counter += 2;
    }
}

impl EventHandler for CHIP8Emulator {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        // Fetch-decode-execute cycle with some other stuff
        let timer = time::Instant::now();
        while timer.elapsed().as_millis() <= 25 {
            let tick_id = (self.internal_timer.elapsed().as_secs_f32() * 60.0) as u64;
            let tick_diff = min(tick_id - self.last_tick, 255) as u8;
            self.last_tick = tick_id;
            self.delay_timer -= min(tick_diff, self.delay_timer);
            self.sound_timer -= min(tick_diff, self.sound_timer);
            if self.sound_timer > 0 && !self.beep_sound.playing() {
                self.beep_sound.play()?;
            }
            if self.sound_timer == 0 && self.beep_sound.playing() {
                self.beep_sound.stop();
            }
            match self.awaiting_status {
                AwaitingStatus::None => {
                    let opcode = self.fetch();
                    let opcode = opcode.into();
                    let start = time::Instant::now();
                    self.execute(ctx, &opcode)?;
                    let time = start.elapsed().as_millis();
                    if time > 1 {
                        println!("[WARNING] executed {:?} in {} millis", opcode, time);
                    }
                },
                _ => break,
            }
        }
        Ok(())
    }

    fn key_down_event(
        &mut self,
        ctx: &mut Context,
        keycode: KeyCode,
        _keymods: KeyMods,
        _repeat: bool
    ) {
        match keycode {
            KeyCode::Escape => event::quit(ctx),
            // consider checking if repeat is true or false
            key => {
                use std::collections::hash_map::Entry::Occupied;
                if let Occupied(entry) = self.keymap_rev.entry(key) {
                    if let AwaitingStatus::Awaiting(reg_id) = self.awaiting_status {
                        self.regs[reg_id] = *entry.get();
                        self.awaiting_status = AwaitingStatus::None;
                    }
                }
            }
        }
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx, [0.0, 0.1, 0.0, 1.0].into());

        let (w, h) = graphics::drawable_size(ctx);
        let (cell_w, cell_h) = (w / COLS as f32, h / ROWS as f32);
        let off = 1.0;

        let mut mesh_builder = &mut graphics::MeshBuilder::new();
        // Draw 64x32 field
        for x in 0..COLS {
            for y in 0..ROWS {
                let (xx, yy) = (x as f32, y as f32);
                let rect = graphics::Rect::new(xx * cell_w + off, yy * cell_h + off, cell_w - 2.0 * off, cell_h - 2.0 * off);
                mesh_builder = mesh_builder.rectangle(
                    graphics::DrawMode::fill(), 
                    rect, 
                    {
                        if self.screen[self.get_pixel(x, y)] {
                            [0.0, 1.0, 0.0, 1.0]
                        } else {
                            [0.0, 0.2, 0.0, 1.0]
                        }
                    }.into()
                );
            }
        }
        let mesh = &mesh_builder.build(ctx).unwrap();
        graphics::draw(ctx, mesh, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;
        graphics::present(ctx)?;

        if let AwaitingStatus::AwaitingScreenUpdate = self.awaiting_status {
            self.awaiting_status = AwaitingStatus::None;
        }
        Ok(())
    }
}