use std::cmp::PartialEq;
// TODO: Change usize of reg IDs to u8
#[derive(Debug, Clone, PartialEq)]
pub enum CHIP8Opcode {
    UnknownOpcode(u16),
    ClearScreen,
    DrawSprite{
        reg_x: usize,
        reg_y: usize,
        height: u8,
    },
    Return,
    Goto(u16),
    CallSubroutine(u16),
    SkipNextIfEqual{
        reg_id: usize,
        value: u8,
    },
    SkipNextIfNEqual{
        reg_id: usize,
        value: u8,
    },
    SkipNextIfRegEqual{
        reg_a: usize,
        reg_b: usize,
    },
    SkipNextIfRegNEqual{
        reg_a: usize,
        reg_b: usize,
    },
    SkipNextIfKeyEqual{
        reg_id: usize,
    },
    SkipNextIfKeyNEqual{
        reg_id: usize,
    },
    AssignValue{
        reg_id: usize,
        value: u8,
    },
    AssignAddValue{
        reg_id: usize,
        value: u8,
    },
    AssignReg{
        reg_a: usize,
        reg_b: usize,
    },
    AssignBitwiseOr{
        reg_a: usize,
        reg_b: usize,
    },
    AssignBitwiseAnd{
        reg_a: usize,
        reg_b: usize,
    },
    AssignBitwiseXor{
        reg_a: usize,
        reg_b: usize,
    },
    AssignAdd{
        reg_a: usize,
        reg_b: usize,
    },
    AssignSub{
        reg_a: usize,
        reg_b: usize,
    },
    AssignBitwiseRShift{
        reg_a: usize,
        reg_b: usize,
    },
    AssignSubReverse{
        reg_a: usize,
        reg_b: usize,
    },
    AssignBitwiseLShift{
        reg_a: usize,
        reg_b: usize,
    },
    AssignDelayTimer{
        reg_id: usize,
    },
    AssignToDelayTimer{
        reg_id: usize,
    },
    AssignToSoundTimer{
        reg_id: usize,
    },
    AssignToI(u16),
    AssignAddToI{
        reg_id: usize,
    },
    AssignGlyphToI{
        reg_id: usize,
    },
    BinaryCodedDecimal{
        reg_id: usize,
    },
    RegDump{
        reg_id: usize,
    },
    RegLoad{
        reg_id: usize,
    },
    AwaitKey{
        reg_id: usize,
    },
    GotoPlusV0(u16),
    RandWithBitwiseAnd{
        reg_id: usize,
        value: u8,
    },
}

impl CHIP8Opcode {
    fn decode(opcode: u16) -> Self {
        use CHIP8Opcode::*;
        let t = (opcode & 0xF000) >> 12;
        let rest = opcode & 0x0FFF;
        assert!(t <= 0xF);
        match t {
            0x0 => {
                match rest {
                    // 0x00E0
                    0x0E0 => ClearScreen,
                    // 0x00EE
                    0x0EE => Return,
                    _ => UnknownOpcode(opcode),
                }
            },
            // 0x1NNN
            0x1 => Goto(rest),
            // 0x2NNN
            0x2 => CallSubroutine(rest),
            // 0x3XNN
            0x3 => {
                let reg_id: usize = ((rest & 0xF00) >> 8) as usize;
                let value: u8 = (rest & 0x0FF) as u8;
                SkipNextIfEqual{
                    reg_id,
                    value,
                }
            },
            // 0x4XNN
            0x4 => {
                let reg_id: usize = ((rest & 0xF00) >> 8) as usize;
                let value: u8 = (rest & 0x0FF) as u8;
                SkipNextIfNEqual{
                    reg_id,
                    value,
                }
            },
            // 0x5XY0
            0x5 => {
                let reg_a: usize = ((rest & 0xF00) >> 8) as usize;
                let reg_b: usize = ((rest & 0x0F0) >> 4) as usize;
                SkipNextIfRegEqual{
                    reg_a,
                    reg_b,
                }
            },
            // 0x6XNN
            0x6 => {
                let reg_id: usize = ((rest & 0xF00) >> 8) as usize;
                let value: u8 = (rest & 0x0FF) as u8;
                AssignValue{
                    reg_id,
                    value,
                }
            },
            // 0x7XNN
            0x7 => {
                let reg_id: usize = ((rest & 0xF00) >> 8) as usize;
                let value: u8 = (rest & 0x0FF) as u8;
                AssignAddValue{
                    reg_id,
                    value,
                }
            },
            0x8 => {
                let reg_a: usize = ((rest & 0xF00) >> 8) as usize;
                let reg_b: usize = ((rest & 0x0F0) >> 4) as usize;
                let t = rest & 0x00F;
                match t {
                    // 0x8XY0
                    0x0 => AssignReg{
                        reg_a,
                        reg_b,
                    },
                    // 0x8XY1
                    0x1 => AssignBitwiseOr{
                        reg_a,
                        reg_b,
                    },
                    // 0x8XY2
                    0x2 => AssignBitwiseAnd{
                        reg_a,
                        reg_b,
                    },
                    // 0x8XY3
                    0x3 => AssignBitwiseXor{
                        reg_a,
                        reg_b,
                    },
                    // 0x8XY4
                    0x4 => AssignAdd{
                        reg_a,
                        reg_b,
                    },
                    // 0x8XY5
                    0x5 => AssignSub{
                        reg_a,
                        reg_b,
                    },
                    // 0x8XY6
                    0x6 => AssignBitwiseRShift{
                        reg_a,
                        reg_b,
                    },
                    // 0x8XY7
                    0x7 => AssignSubReverse{
                        reg_a,
                        reg_b,
                    },
                    // 0x8XYE
                    0xE => AssignBitwiseLShift{
                        reg_a,
                        reg_b,
                    },
                    _ => UnknownOpcode(opcode),
                }
            },
            // 0x9XY0
            0x9 => {
                let reg_a: usize = ((rest & 0xF00) >> 8) as usize;
                let reg_b: usize = ((rest & 0x0F0) >> 4) as usize;
                SkipNextIfRegNEqual{
                    reg_a,
                    reg_b,
                }
            },
            // 0xANNN
            0xA => AssignToI(rest),
            // 0xBNNN
            0xB => GotoPlusV0(rest),
            // 0xCXNN
            0xC => {
                let reg_id: usize = ((rest & 0xF00) >> 8) as usize;
                let value: u8 = (rest & 0x0FF) as u8;
                RandWithBitwiseAnd{
                    reg_id,
                    value,
                }
            },
            // 0xDXYN
            0xD => {
                let reg_x: usize = ((rest & 0xF00) >> 8) as usize;
                let reg_y: usize = ((rest & 0x0F0) >> 4) as usize;
                let height: u8 = (rest & 0x00F) as u8;
                DrawSprite{
                    reg_x,
                    reg_y,
                    height
                }
            },
            0xE => {
                let reg_id: usize = ((rest & 0xF00) >> 8) as usize;
                
                let t = rest & 0x0FF;
                match t {
                    // 0xEX9E
                    0x9E => SkipNextIfKeyEqual{
                        reg_id
                    },
                    // 0xEXA1
                    0xA1 => SkipNextIfKeyNEqual{
                        reg_id
                    },
                    _ => UnknownOpcode(opcode),
                }
            },
            0xF => {
                let reg_id: usize = ((rest & 0xF00) >> 8) as usize;
                let t = rest & 0x0FF;
                match t {
                    // 0xFX07
                    0x07 => AssignDelayTimer{
                        reg_id
                    },
                    // 0xFX0A
                    0x0A => AwaitKey{
                        reg_id,
                    },
                    // 0xFX15
                    0x15 => AssignToDelayTimer{
                        reg_id,
                    },
                    // 0xFX18
                    0x18 => AssignToSoundTimer{
                        reg_id,
                    },
                    // 0xFX1E
                    0x1E => AssignAddToI{
                        reg_id,
                    },
                    // 0xFX29
                    0x29 => AssignGlyphToI{
                        reg_id,
                    },
                    // 0xFX33
                    0x33 => BinaryCodedDecimal{
                        reg_id,
                    },
                    // 0xFX55
                    0x55 => RegDump{
                        reg_id,
                    },
                    // 0xFX65
                    0x65 => RegLoad{
                        reg_id,
                    },
                    _ => UnknownOpcode(opcode),
                }
            },
            _ => UnknownOpcode(opcode),
        }
    }
}

impl From<u16> for CHIP8Opcode {
    fn from(opcode: u16) -> Self {
        CHIP8Opcode::decode(opcode)
    }
}

// #[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_0x6300() {
        assert_eq!(CHIP8Opcode::AssignValue{reg_id: 3, value: 00}, 0x6300.into());
    }
}